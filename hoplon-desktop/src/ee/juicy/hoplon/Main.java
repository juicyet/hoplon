package ee.juicy.hoplon;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;

public class Main {
	public static void main(String[] args) {
		new LwjglApplication(new Hoplon(), "Hoplon", 320, 480, false);
	}
}
