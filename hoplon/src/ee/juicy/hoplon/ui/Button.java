package ee.juicy.hoplon.ui;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;


public class Button{
	public TextureWrapper BackTexture;
	public TextureWrapper ClickedTexture;
	public TextWrapper Text;
	public Vector2 Position;
	public Boolean IsClicked;

	public Button(TextureWrapper backTex,TextureWrapper clickTex,String text,Vector2 pos){
		BackTexture=backTex;
		ClickedTexture=clickTex;
		Text=new TextWrapper(text,pos);
		Position=pos;
		IsClicked=false;
	}

	public void Draw(SpriteBatch sp,BitmapFont fnt){
		if(!IsClicked){
			BackTexture.Draw(sp);
		}else{
			ClickedTexture.Draw(sp);
		}
		Text.Draw(sp,fnt);
	} 

	public void Update(int tapx,int tapy){
		IsClicked=IsInside(tapx,tapy);
	} 

	Boolean IsInside(int x,int y){
		int px=(int)(BackTexture.Position.x-BackTexture.destWidth/2);
		int py=(int)(BackTexture.Position.y-BackTexture.destHeight/2);
		int width=(int)(BackTexture.Position.x+BackTexture.destWidth/2);
		int height=(int)(BackTexture.Position.y+BackTexture.destHeight/2);
		if(x>=px && x<=(px+width) && y>=py && y<=(py+height))
			return true;
		return false;
	} 
}