package ee.juicy.hoplon.ui;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class TextureWrapper{
	public Texture texture;
	public Vector2 Position;
	int srcX;
	int srcY;
	int srcWidth;
	int srcHeight;
	int destWidth;
	int destHeight;
	public boolean IsActive;
	public TextureWrapper(TextureRegion tex,Vector2 pos,int destinationWidth,int destinationHeight)
	{

		texture=tex.getTexture();
		srcX=tex.getRegionX();
		srcY=tex.getRegionY();
		srcWidth=tex.getRegionWidth();
		srcHeight=tex.getRegionHeight();
		destWidth=destinationWidth;
		destHeight=destinationHeight;
		Position=pos;
		IsActive=true;

	}

	public void SetTexture(TextureRegion region)
	{
		texture=region.getTexture();
		srcX=region.getRegionX();
		srcY=region.getRegionY();
		srcWidth=region.getRegionWidth();
		srcHeight=region.getRegionHeight();

	}

	public void Draw(SpriteBatch sp)
	{
		if(IsActive)
		{
			sp.draw(texture, Position.x-destWidth/2, //Position.x-destWidth/2 gives the center of texture in x axis
					Position.y-destHeight/2, //Position.y-destHeight/2 gives center of texture in y axis
					destWidth,destHeight,srcX,srcY,
					srcWidth,srcHeight,false,false);
		}
	}

}