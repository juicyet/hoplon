package ee.juicy.hoplon.view;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

import ee.juicy.hoplon.Hoplon;
import ee.juicy.hoplon.model.Bullet;
import ee.juicy.hoplon.model.Cell;
import ee.juicy.hoplon.model.Enemy;
import ee.juicy.hoplon.model.Player;
import ee.juicy.hoplon.model.World;
import ee.juicy.hoplon.ui.Button;

public class WorldRenderer {


	private World world;
	private OrthographicCamera cam;
	SpriteBatch spriteBatch;
	BitmapFont font;
	String scoreString = "Score: ";
	Texture texture;

	/** for debug rendering **/
	ShapeRenderer debugRenderer = new ShapeRenderer();


	private boolean debug = false;
	private int width;
	private int height;

	Button testButton;
	@SuppressWarnings("unused")
	private Texture safeTexture;
	private Texture vulnerableTexture;
	private Texture enemyTexture;
	private Texture playerSafeTexture;
	private Texture playerDangerTexture;
	private Texture bckgndTexture;

	private Sprite playerSafeSprite;
	private Sprite playerDangerSprite;
	@SuppressWarnings("unused")
	private Sprite enemySprite;

	private Pixmap pixmap;

	private boolean wasChanged;
	@SuppressWarnings("unused")
	private boolean isFirst;
	

	private Cell[][] worldCells;

	TextureRegion[][] tiles;

	private Player player;
	private ArrayList<Enemy> worldEnemies;
	private CopyOnWriteArrayList<Bullet> worldBullets;
	private Texture bulletTexture;

	public void setSize (int w, int h) {
		this.width = w;
		this.height = h;
	}

	public WorldRenderer(World world, boolean debug) {
		this.world = world;
		this.cam = new OrthographicCamera(Hoplon.SCREEN_WIDTH, Hoplon.SCREEN_HEIGHT);
		this.cam.position.set(Hoplon.SCREEN_WIDTH/2, Hoplon.SCREEN_HEIGHT/2, 0);
		this.debug = false;
		this.wasChanged = false;
		this.isFirst = true;
		this.font = new BitmapFont(Gdx.files.internal("data/hoplon_font.fnt"),
				Gdx.files.internal("data/hoplon_font.png"), false);
		loadTextures();
		this.worldCells = world.getCells();
		this.worldEnemies = world.getEnemies();
		
		this.spriteBatch = new SpriteBatch(1000);
		this.pixmap = new Pixmap(Gdx.files.internal("images/bckgnd3.png"));

		//this.fbo = new FrameBuffer(Format.RGB565, Hoplon.ARENA_WIDTH, Hoplon.ARENA_HEIGHT, false);
	}


	public void render() {
		//cam.setToOrtho(false, width, height);
		world.setWidth(width);
		world.setHeight(height);
		cam.update();

		if (debug) {
			drawDebug();
		}
		else {
		
			draw();
		}
	}




	private void draw() {	
		worldBullets = world.getBullets();
		spriteBatch.begin();
		
		for(int i = 0; i < Hoplon.ARENA_HEIGHT/2; i++) {
			for(int j = 0; j < Hoplon.ARENA_WIDTH/2; j++) {
				if(worldCells[i][j].getState() == Cell.State.SAFE && !(worldCells[i][j].isDrawn())) {
					//pixmap.getPixel(j,i);
					pixmap.drawPixel(j*Cell.SIZE, i*Cell.SIZE, Color.rgba8888(1,1,1,1.0f));
					pixmap.drawPixel(j*Cell.SIZE+1, i*Cell.SIZE+1, Color.rgba8888(1,1,1,1.0f));
					pixmap.drawPixel(j*Cell.SIZE+1, i*Cell.SIZE, Color.rgba8888(1,1,1,1.0f));
					pixmap.drawPixel(j*Cell.SIZE, i*Cell.SIZE+1, Color.rgba8888(1,1,1,1.0f));

					worldCells[i][j].setDrawn(true);
					wasChanged = true;
				}


			}
		}
		//System.out.println(dangerCounter);
		if(wasChanged) {
			bckgndTexture = new Texture(pixmap, false);
			wasChanged = false;
		}




		spriteBatch.disableBlending();

		//spriteBatch.draw(bckgndTexture, World.LEFT_X, World.BOTTOM_Y);
		spriteBatch.draw(bckgndTexture, World.LEFT_X, World.BOTTOM_Y, Hoplon.ARENA_WIDTH, Hoplon.ARENA_HEIGHT, 0, 0, Hoplon.ARENA_WIDTH, Hoplon.ARENA_HEIGHT, false, true);
		font.draw(spriteBatch, String.valueOf(world.getPlayer().getScore()), World.LEFT_X, World.BOTTOM_Y);
		font.draw(spriteBatch, "Level: " + String.valueOf(world.getDifficultyLevel()), World.LEFT_X, World.BOTTOM_Y-32);
		font.draw(spriteBatch, (double)Math.round((double)world.getRoundScore() / (double)world.getCellCount()*1000)/10 + "%", World.LEFT_X + 128, World.BOTTOM_Y-32);
		font.draw(spriteBatch, "Deaths: " + String.valueOf(world.getPlayer().getNumOfDeaths()), World.LEFT_X + 128, World.BOTTOM_Y);
		spriteBatch.enableBlending();
		
		player = world.getPlayer();
		spriteBatch.setColor(Color.RED);
		if(player.getState() == Player.State.DANGER) {
			for(int i = 0; i < Hoplon.ARENA_HEIGHT/2; i++) {
				for(int j = 0; j < Hoplon.ARENA_WIDTH/2; j++) {
					if (worldCells[i][j].getState() == Cell.State.VULNERABLE){
						
						spriteBatch.draw(vulnerableTexture, 
								World.LEFT_X + worldCells[i][j].x * Cell.SIZE, 
								World.BOTTOM_Y + worldCells[i][j].y * Cell.SIZE, 
								Cell.SIZE, Cell.SIZE);
					}
				}
			}
		}
		spriteBatch.setColor(Color.WHITE);






		//		for (Cell[] cells : worldCells) {
		//			for(Cell cell : cells) {
		//				if(cell != null) {
		//					if(cell.getState() == Cell.State.SAFE) { 
		//
		//						spriteBatch.draw(safeTexture, World.LEFT_X + cell.x * Cell.SIZE, World.BOTTOM_Y + cell.y * Cell.SIZE, 
		//								Cell.SIZE, Cell.SIZE);
		//
		//					}
		//					else if(cell.getState() == Cell.State.VULNERABLE) {
		//						spriteBatch.draw(vulnerableTexture, World.LEFT_X + cell.x * Cell.SIZE, World.BOTTOM_Y + cell.y * Cell.SIZE, 
		//								Cell.SIZE, Cell.SIZE);
		//					}
		//				}
		//			}
		//		}   



		

		if(player.getState() != Player.State.DANGER) {
			playerSafeSprite.setPosition(World.LEFT_X + player.getPosition().x * Cell.SIZE - Player.SIZE/2 + 1, 
					World.BOTTOM_Y + player.getPosition().y * Cell.SIZE  - Player.SIZE/2 + 1);
			if(player.getDirection() == Player.Direction.LEFT) {
				playerSafeSprite.setRotation(90);
			}
			else if(player.getDirection() == Player.Direction.RIGHT) {
				playerSafeSprite.setRotation(270);
			}
			else if(player.getDirection() == Player.Direction.UP) {
				playerSafeSprite.setRotation(0);
			}
			else {
				playerSafeSprite.setRotation(180);
			}

			playerSafeSprite.draw(spriteBatch);
		}
		else {
			playerDangerSprite.setPosition(World.LEFT_X + player.getPosition().x * Cell.SIZE - Player.SIZE/2 + 1, 
					World.BOTTOM_Y + player.getPosition().y * Cell.SIZE  - Player.SIZE/2 + 1);
			playerDangerSprite.draw(spriteBatch);
			if(player.getDirection() == Player.Direction.LEFT) {
				playerDangerSprite.setRotation(90);
			}
			else if(player.getDirection() == Player.Direction.RIGHT) {
				playerDangerSprite.setRotation(270);
			}
			else if(player.getDirection() == Player.Direction.UP) {
				playerDangerSprite.setRotation(0);
			}
			else {
				playerDangerSprite.setRotation(180);
			}

		}

		for(Enemy enemy : worldEnemies) {
			spriteBatch.draw(enemyTexture, 
					World.LEFT_X + enemy.getPosition().x * Cell.SIZE - Enemy.SIZE/2, 
					World.BOTTOM_Y + enemy.getPosition().y * Cell.SIZE - Enemy.SIZE/2, 
					Enemy.SIZE, Enemy.SIZE);
		}
		
		for(Bullet bullet : worldBullets) {
			spriteBatch.draw(bulletTexture, 
					World.LEFT_X + bullet.getPosition().x * Cell.SIZE - Cell.SIZE/2, 
					World.BOTTOM_Y + bullet.getPosition().y * Cell.SIZE - Cell.SIZE/2, 
					Cell.SIZE, Cell.SIZE);
		}
		spriteBatch.end();		
	}

	private void loadTextures() {
		safeTexture = new  Texture(Gdx.files.internal("images/safe.png"));
		vulnerableTexture = new Texture(Gdx.files.internal("images/vulnerable.png"));
		enemyTexture = new Texture(Gdx.files.internal("images/enemy.png"));
		bulletTexture = new Texture(Gdx.files.internal("images/bullet.png"));
		playerSafeTexture = new Texture(Gdx.files.internal("images/playerSafe.png"));
		playerDangerTexture = new Texture(Gdx.files.internal("images/playerDanger.png"));
		//bckgndTexture = new Texture(Gdx.files.internal("images/bckgnd.png"));;

		playerSafeSprite = new Sprite(playerSafeTexture);
		playerDangerSprite = new Sprite(playerDangerTexture);
		enemySprite = new Sprite(enemyTexture);

		//tiles = TextureRegion.split(bckgndTexture, 2, 2);

	}

	private void drawDebug() {
		spriteBatch = new SpriteBatch();

		//font.setColor(1.0f, 1.0f, 1.0f, 1.0f);


		spriteBatch.begin();
		player = world.getPlayer();
		//spriteBatch.draw(bckgndTexture, World.LEFT_X, World.BOTTOM_Y, Hoplon.ARENA_WIDTH, Hoplon.ARENA_HEIGHT);
		if(player.getState() != Player.State.DANGER) {
			playerSafeSprite.setPosition(World.LEFT_X + player.getPosition().x * Cell.SIZE - Player.SIZE/2 + 1, 
					World.BOTTOM_Y + player.getPosition().y * Cell.SIZE  - Player.SIZE/2 + 1);
			if(player.getDirection() == Player.Direction.LEFT) {
				playerSafeSprite.setRotation(90);
			}
			else if(player.getDirection() == Player.Direction.RIGHT) {
				playerSafeSprite.setRotation(270);
			}
			else if(player.getDirection() == Player.Direction.UP) {
				playerSafeSprite.setRotation(0);
			}
			else {
				playerSafeSprite.setRotation(180);
			}

			playerSafeSprite.draw(spriteBatch);
		}
		else {
			playerDangerSprite.setPosition(World.LEFT_X + player.getPosition().x * Cell.SIZE - Player.SIZE/2 + 1, 
					World.BOTTOM_Y + player.getPosition().y * Cell.SIZE  - Player.SIZE/2 + 1);
			playerDangerSprite.draw(spriteBatch);
			if(player.getDirection() == Player.Direction.LEFT) {
				playerDangerSprite.setRotation(90);
			}
			else if(player.getDirection() == Player.Direction.RIGHT) {
				playerDangerSprite.setRotation(270);
			}
			else if(player.getDirection() == Player.Direction.UP) {
				playerDangerSprite.setRotation(0);
			}
			else {
				playerDangerSprite.setRotation(180);
			}

		}

		// render the enemy
		for(Enemy enemy : worldEnemies) {
			spriteBatch.draw(enemyTexture, World.LEFT_X + enemy.getPosition().x * Cell.SIZE - Enemy.SIZE/2, World.BOTTOM_Y + enemy.getPosition().y * Cell.SIZE - Enemy.SIZE/2, 
					Enemy.SIZE, Enemy.SIZE);
		}



		font.draw(spriteBatch, scoreString + world.getPlayer().getScore(), World.LEFT_X, World.BOTTOM_Y);
		spriteBatch.end();
		spriteBatch.dispose();
		// render blocks

		debugRenderer.setProjectionMatrix(cam.combined);
		debugRenderer.begin(ShapeType.FilledRectangle);

		for (Cell[] cells : worldCells) {
			for(Cell cell : cells) {
				if(cell != null) {
					if(cell.getState() == Cell.State.SAFE) {
						debugRenderer.setColor(new Color(1, 1, 1, 1));
						debugRenderer.filledRect(World.LEFT_X + cell.x * cell.width, World.BOTTOM_Y + cell.y * cell.height, cell.width, cell.height);
					}
					else if(cell.getState() == Cell.State.VULNERABLE) {
						debugRenderer.setColor(new Color(1f, 0.5f, 0, 1));						
						debugRenderer.filledRect(World.LEFT_X + cell.x * cell.width, World.BOTTOM_Y + cell.y * cell.height, cell.width, cell.height);
					}
				}
			}
		}
		// render the player

		//		if(player.getState() == Player.State.MOVING || player.getState() == Player.State.IDLE) {
		//			debugRenderer.setColor(new Color(0, 1, 0, 1));
		//		}
		//		else {
		//			debugRenderer.setColor(new Color(1, 1, 0, 1));
		//		}
		//
		//		debugRenderer.filledRect(World.LEFT_X + player.getPosition().x * Cell.SIZE, World.BOTTOM_Y + player.getPosition().y * Cell.SIZE, Cell.SIZE, Cell.SIZE);
		//
		//
		//
		//		// render the enemy
		//		debugRenderer.setColor(new Color(1, 0, 0, 1));
		//		for(Enemy enemy : worldEnemies) {
		//			debugRenderer.filledRect(World.LEFT_X + enemy.getPosition().x * Cell.SIZE, World.BOTTOM_Y + enemy.getPosition().y * Cell.SIZE, Cell.SIZE, Cell.SIZE);
		//		}
		debugRenderer.end();
	}



}
