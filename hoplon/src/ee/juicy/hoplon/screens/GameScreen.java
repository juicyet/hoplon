package ee.juicy.hoplon.screens;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL10;

import ee.juicy.hoplon.Hoplon;
import ee.juicy.hoplon.controller.WorldController;
import ee.juicy.hoplon.model.World;
import ee.juicy.hoplon.view.WorldRenderer;

public class GameScreen implements Screen, InputProcessor {

	private World 			world;
	private WorldRenderer 	renderer;
	private WorldController	controller;
	@SuppressWarnings("unused")
	private Hoplon 			game;
	private int 			difficultyLevel;
	private int 			globalScore;
	private int 			globalNumOfDeaths;
	Music 					bckgndMusic; //TODO Implement a custom MusicPlayer class to manage music
	
	public GameScreen(Hoplon game) {
		this.game = game;
	}

	@Override
	public void show() {
		difficultyLevel = 1;
		globalScore = 0;
		globalNumOfDeaths = 0;
		world = new World(difficultyLevel);
		renderer = new WorldRenderer(world, false);
		controller = new WorldController(world);
		Gdx.input.setInputProcessor(this);
		bckgndMusic = Gdx.audio.newMusic(Gdx.files.internal("music/game.mp3"));
		bckgndMusic.setVolume(0.5f);
		bckgndMusic.setLooping(true);
		bckgndMusic.play();
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		controller.update();
		renderer.render();
		//If one game/round is over, initialize a new world, but retain the score/death count
		//TODO This really needs a more elegant solution...
		if(controller.isGameOver()) {
			difficultyLevel++;
			globalScore = world.getPlayer().getScore();
			globalNumOfDeaths += world.getPlayer().getNumOfDeaths();
			world = new World(difficultyLevel);
			world.getPlayer().setScore(globalScore);
			world.getPlayer().setNumOfDeaths(globalNumOfDeaths);
			renderer = new WorldRenderer(world,  true);
			controller = new WorldController(world);
		}
	}
	
	@Override
	public void resize(int width, int height) {
		renderer.setSize(width, height);
	}

	@Override
	public void hide() {
		Gdx.input.setInputProcessor(null);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
	}

	@Override
	public void dispose() {
		Gdx.input.setInputProcessor(null);
	}

	// * InputProcessor methods ***************************//

	@Override
	public boolean keyDown(int keycode) {
		if (keycode == Keys.LEFT)
			controller.leftPressed();
		if (keycode == Keys.RIGHT)
			controller.rightPressed();
		if (keycode == Keys.UP)
			controller.upPressed();
		if (keycode == Keys.DOWN)
			controller.downPressed();
		if (keycode == Keys.SHIFT_LEFT || keycode == Keys.SHIFT_RIGHT)
			controller.shiftPressed();
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		if (keycode == Keys.LEFT)
			controller.leftReleased();
		if (keycode == Keys.RIGHT)
			controller.rightReleased();
		if (keycode == Keys.UP)
			controller.upReleased();
		if (keycode == Keys.DOWN)
			controller.downReleased();
//		if (keycode == Keys.SHIFT_LEFT || keycode == Keys.SHIFT_RIGHT)
//			controller.shiftReleased();
		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		if(character == 'm' || character == 'M') {
			if(bckgndMusic.isPlaying()) {
				bckgndMusic.stop();
			}
			else {
				bckgndMusic.play();
			}
		}
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		if (!Gdx.app.getType().equals(ApplicationType.Android))
			return false;
		y = Math.abs(y-Hoplon.SCREEN_HEIGHT);
		if (x < World.LEFT_X+30 && y < World.TOP_Y && y > World.BOTTOM_Y) {
			controller.leftPressed();
		}
		if (x > World.RIGHT_X-30 && y < World.TOP_Y && y > World.BOTTOM_Y) {
			controller.rightPressed();
		}
		if (y > World.TOP_Y-30 && x < World.RIGHT_X && x > World.LEFT_X) {
			controller.upPressed();
		}
		if (y < World.BOTTOM_Y+30 && x < World.RIGHT_X && x > World.LEFT_X) {
			controller.downPressed();
		}
		return true;
	}

	@Override
	public boolean touchUp(int x, int y, int pointer, int button) {
		if (!Gdx.app.getType().equals(ApplicationType.Android))
			return false;
		y = Math.abs(y-Hoplon.SCREEN_HEIGHT);
		if (x < World.LEFT_X && y < World.TOP_Y && y > World.BOTTOM_Y) {
			controller.leftReleased();
		}
		if (x > World.RIGHT_X && y < World.TOP_Y && y > World.BOTTOM_Y) {
			controller.rightReleased();
		}
		if (y > World.TOP_Y && x < World.RIGHT_X && x > World.LEFT_X) {
			controller.upReleased();
		}
		if (y < World.BOTTOM_Y && x < World.RIGHT_X && x > World.LEFT_X) {
			controller.downReleased();
		}
		
		return true;
	}

	@Override
	public boolean touchDragged(int x, int y, int pointer) {
		if (!Gdx.app.getType().equals(ApplicationType.Android))
			return false;
		controller.shiftPressed();
		return true;
	}

	@Override
	public boolean touchMoved(int x, int y) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
	

}
