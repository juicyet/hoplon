package ee.juicy.hoplon.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import ee.juicy.hoplon.Hoplon;

public class MenuScreen implements Screen, InputProcessor {

	Hoplon game;
	SpriteBatch spriteBatch;
	Texture startBannerTexture;
	Music bckgndMusic; //TODO Implement a custom MusicPlayer class to manage music
	
	// constructor to keep a reference to the main Game class
	public MenuScreen(Hoplon game){
		this.game = game;
		
	}

	@Override
	public void render(float delta) {
		startBannerTexture =  new  Texture(Gdx.files.internal("images/start.png"));
		startBannerTexture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
		spriteBatch = new SpriteBatch();
		
		spriteBatch.begin();
		spriteBatch.draw(startBannerTexture, Hoplon.SCREEN_WIDTH/2 - startBannerTexture.getWidth()/2,
				Hoplon.SCREEN_HEIGHT/2 - startBannerTexture.getHeight()/2, 
				startBannerTexture.getWidth(), startBannerTexture.getHeight());
		spriteBatch.end();
		spriteBatch.dispose();
		// update and draw stuff
		if (Gdx.input.isButtonPressed(Buttons.LEFT)) // use your own criterion here
			game.setScreen(game.gameScreen);
	}

	@Override
	public void resize(int width, int height) {
	}


	@Override
	public void show() {
		// called when this screen is set as the screen with game.setScreen();
		Gdx.input.setInputProcessor(this);
		bckgndMusic = Gdx.audio.newMusic(Gdx.files.internal("music/menu.mp3"));
		bckgndMusic.setLooping(true);
		bckgndMusic.setVolume(0.5f);
		bckgndMusic.play();
	}


	@Override
	public void hide() {
		bckgndMusic.stop();
		bckgndMusic.dispose();
		Gdx.input.setInputProcessor(null);
		// called when current screen changes from this to a different screen
	}


	@Override
	public void pause() {
	}


	@Override
	public void resume() {
	}


	@Override
	public void dispose() {
		spriteBatch.dispose();
		startBannerTexture.dispose();
		bckgndMusic.stop();
		bckgndMusic.dispose();
		Gdx.input.setInputProcessor(null);
		// never called automatically
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int x, int y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int x, int y, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchMoved(int x, int y) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		if(character == 'm' || character == 'M') {
			if(bckgndMusic.isPlaying()) {
				bckgndMusic.stop();
			}
			else {
				bckgndMusic.play();
			}
		}
		// TODO Auto-generated method stub
		return false;
	}

}
