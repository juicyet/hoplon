package ee.juicy.hoplon;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.Texture;

import ee.juicy.hoplon.screens.GameScreen;
import ee.juicy.hoplon.screens.MenuScreen;

public class Hoplon extends Game implements ApplicationListener{
	
	public static final int SCREEN_HEIGHT = 480;
	public static final int SCREEN_WIDTH = 320;
	
	public static final int ARENA_WIDTH = 240;
	public static final int ARENA_HEIGHT = 360;
	
	public MenuScreen menuScreen;
    public GameScreen gameScreen;
    
	@Override
	public void create() {
		Texture.setEnforcePotImages(false);
		menuScreen = new MenuScreen(this);
		gameScreen = new GameScreen(this);
		setScreen(menuScreen);
	}
}
