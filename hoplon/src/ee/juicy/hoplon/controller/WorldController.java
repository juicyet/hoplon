package ee.juicy.hoplon.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

import ee.juicy.hoplon.Hoplon;
import ee.juicy.hoplon.model.Bullet;
import ee.juicy.hoplon.model.Cell;
import ee.juicy.hoplon.model.Enemy;
import ee.juicy.hoplon.model.Player;
import ee.juicy.hoplon.model.World;

/**
 * @author Ott Madis Ozolit
 *
 * Handles all game logic - movement, collision, etc. based on user input and current game state.
 */
public class WorldController {

	enum Keys {
		LEFT, RIGHT, UP, DOWN, SHIFT
	}

	private World 	world;
	private Player 	player;
	private boolean gameOver = false;

	//Keys are mapped in GameScreen.java
	static Map<Keys, Boolean> keys = new HashMap<WorldController.Keys, Boolean>();
	static {
		keys.put(Keys.LEFT, false);
		keys.put(Keys.RIGHT, false);
		keys.put(Keys.UP, false);
		keys.put(Keys.DOWN, false);
		keys.put(Keys.SHIFT, false);
	};

	//DUPLICATE IN WORLD.JAVA!!!
	int bottomY = (Hoplon.SCREEN_HEIGHT - Hoplon.ARENA_HEIGHT)/2;
	int topY = (Hoplon.SCREEN_HEIGHT - Hoplon.ARENA_HEIGHT)/2 + Hoplon.ARENA_HEIGHT;
	int leftX = (Hoplon.SCREEN_WIDTH - Hoplon.ARENA_WIDTH)/2;
	int rightX = (Hoplon.SCREEN_WIDTH - Hoplon.ARENA_WIDTH)/2 + Hoplon.ARENA_WIDTH;

	public WorldController(World world) {
		this.world = world;
		this.player = world.getPlayer();
	}

	// ** Key presses and touches **************** //

	public void leftPressed() {
		keys.get(keys.put(Keys.LEFT, true));

	}

	public void rightPressed() {
		keys.get(keys.put(Keys.RIGHT, true));

	}

	public void upPressed() {
		keys.get(keys.put(Keys.UP, true));

	}

	public void downPressed() {
		keys.get(keys.put(Keys.DOWN, true));

	}

	/**
	 * Toggle shift key, is necessary to draw and also saves the last safe position of the player.
	 */
	public void shiftPressed() {
		if(keys.get(Keys.SHIFT) && player.getState() != Player.State.DANGER) {
			keys.get(keys.put(Keys.SHIFT, false));

		}
		else {
			keys.get(keys.put(Keys.SHIFT, true));
			if(player.getLastSafePosition() == null) {
				player.setLastSafePosition(new Vector2(player.getPosition().x, player.getPosition().y));
			}
		}

	}

	public void leftReleased() {

		keys.get(keys.put(Keys.LEFT, false));


	}

	public void rightReleased() {

		keys.get(keys.put(Keys.RIGHT, false));


	}

	public void upReleased() {

		keys.get(keys.put(Keys.UP, false));

	}

	public void downReleased() {

		keys.get(keys.put(Keys.DOWN, false));

	}

	//	public void shiftReleased() {
	//		keys.get(keys.put(Keys.SHIFT, false));
	//
	//	}


	/**
	 * Main method for updating game state
	 * 
	 * Method which updates game state according to current user input and game state.
	 * Moves the player, enemies, checks for collisions, fills in areas drawn by player.
	 */
	public void update() {
		Cell[][] cells = world.getCells();
		processInput(cells);
		handleEnemies(world.getEnemies(), cells);
		
	}


	/**
	 * Processes user input.
	 * 
	 * Moves the user to a specified direction (if any) and sets player state according to current location.
	 * 
	 * @param cells Array of world grid cells
	 */
	private void processInput(Cell[][] cells) {

		if ((keys.get(Keys.LEFT) && keys.get(Keys.RIGHT)) || (keys.get(Keys.UP) && keys.get(Keys.DOWN)) ||
				(keys.get(Keys.LEFT) && keys.get(Keys.UP)) || (keys.get(Keys.LEFT) && keys.get(Keys.DOWN)) ||
				(keys.get(Keys.RIGHT) && keys.get(Keys.DOWN)) || (keys.get(Keys.UP) && keys.get(Keys.RIGHT)) ||
				(!keys.get(Keys.LEFT) &&
						!(keys.get(Keys.RIGHT)) && 
						!(keys.get(Keys.UP)) && 
						!(keys.get(Keys.DOWN)))) {
			if(player.getState() != Player.State.DANGER) {
				player.setState(Player.State.IDLE);
			}
			return;
		}


		else if (keys.get(Keys.LEFT)) {
			player.setDirection(Player.Direction.LEFT);
			if(player.getPosition().x - 1 >= 0 ) {
				if(cells[(int) player.getPosition().y][(int) player.getPosition().x-1].getState() == Cell.State.VULNERABLE) {
					return;
				}
				if(cells[(int) player.getPosition().y][(int) player.getPosition().x-1].getState() == Cell.State.SAFE) {
					if(player.getState() == Player.State.DANGER) {
						for(Enemy enemy : world.getEnemies()) {
							floodFill(cells, new Vector2(enemy.getPosition().y, enemy.getPosition().x));

						}
						scoreCells(cells);
					}
					player.getPosition().x += -Player.SPEED;
					Gdx.graphics.getDeltaTime();
					keys.get(keys.put(Keys.SHIFT, false));
				}
				else if(keys.get(Keys.SHIFT)) {
					player.getPosition().x += -Player.SPEED;
					cells[(int) player.getPosition().y][(int) player.getPosition().x].setState(Cell.State.VULNERABLE);
				}

				if(cells[(int) player.getPosition().y][(int) player.getPosition().x].getState() != Cell.State.SAFE) {
					player.setState(Player.State.DANGER);
				}
				else {
					player.setState(Player.State.MOVING);
				}
			}



		}
		if (keys.get(Keys.RIGHT)) {
			player.setDirection(Player.Direction.RIGHT);
			if(player.getPosition().x + 1 < Hoplon.ARENA_WIDTH/2 ) {

				if(cells[(int) player.getPosition().y][(int) player.getPosition().x+1].getState() == Cell.State.VULNERABLE) {
					return;
				}

				if(cells[(int) player.getPosition().y][(int) player.getPosition().x+1].getState() == Cell.State.SAFE) {
					if(player.getState() == Player.State.DANGER) {
						for(Enemy enemy : world.getEnemies()) {
							floodFill(cells, new Vector2(enemy.getPosition().y, enemy.getPosition().x));
						}
						scoreCells(cells);
					}
					player.getPosition().x += Player.SPEED;
					keys.get(keys.put(Keys.SHIFT, false));
				}
				else if(keys.get(Keys.SHIFT)) {
					player.getPosition().x += Player.SPEED;
					cells[(int) player.getPosition().y][(int) player.getPosition().x].setState(Cell.State.VULNERABLE);
				}

				if(cells[(int) player.getPosition().y][(int) player.getPosition().x].getState() != Cell.State.SAFE) {
					player.setState(Player.State.DANGER);
				}
				else {
					player.setState(Player.State.MOVING);
				}
			}


		}
		if(keys.get(Keys.UP)) {
			player.setDirection(Player.Direction.UP);
			if(player.getPosition().y + 1 < Hoplon.ARENA_HEIGHT/2 ) {
				if(cells[(int) player.getPosition().y+1][(int) player.getPosition().x].getState() == Cell.State.VULNERABLE) {
					return;
				}

				if(cells[(int) player.getPosition().y+1][(int) player.getPosition().x].getState() == Cell.State.SAFE) {
					if(player.getState() == Player.State.DANGER) {
						for(Enemy enemy : world.getEnemies()) {
							floodFill(cells, new Vector2(enemy.getPosition().y, enemy.getPosition().x));
						}
						scoreCells(cells);

					}
					player.getPosition().y += Player.SPEED;
					keys.get(keys.put(Keys.SHIFT, false));
				}
				else if(keys.get(Keys.SHIFT)) {
					player.getPosition().y += Player.SPEED;
					cells[(int) player.getPosition().y][(int) player.getPosition().x].setState(Cell.State.VULNERABLE);
				}

				if(cells[(int) player.getPosition().y][(int) player.getPosition().x].getState() != Cell.State.SAFE) {
					player.setState(Player.State.DANGER);
				}
				else {
					player.setState(Player.State.MOVING);
				}
			}



		}
		if(keys.get(Keys.DOWN)) {
			player.setDirection(Player.Direction.DOWN);
			if(player.getPosition().y - 1 >= 0 ) {
				if(cells[(int) player.getPosition().y-1][(int) player.getPosition().x].getState() == Cell.State.VULNERABLE) {
					return;
				}

				if(cells[(int) player.getPosition().y-1][(int) player.getPosition().x].getState() == Cell.State.SAFE) {
					if(player.getState() == Player.State.DANGER) {
						for(Enemy enemy : world.getEnemies()) {
							floodFill(cells, new Vector2(enemy.getPosition().y, enemy.getPosition().x));
						}
						scoreCells(cells);
					}
					player.getPosition().y += -Player.SPEED;
					keys.get(keys.put(Keys.SHIFT, false));
				}
				else if(keys.get(Keys.SHIFT)) {
					player.getPosition().y += -Player.SPEED;
					cells[(int) player.getPosition().y][(int) player.getPosition().x].setState(Cell.State.VULNERABLE);
				}

				if(cells[(int) player.getPosition().y][(int) player.getPosition().x].getState() != Cell.State.SAFE) {
					player.setState(Player.State.DANGER);
				}
				else {
					player.setState(Player.State.MOVING);
				}
			}
		}
	}

	/**
	 * Marks in areas where enemies are and sets the cells as vulnerable.
	 * 
	 * @param cells Array of world grid cells
	 * @param pt Enemy's location
	 */
	private void floodFill(Cell[][] cells, Vector2 pt) {
		Queue<Vector2> q = new LinkedList<Vector2>();
		q.add(pt);
		while (q.size() > 0) {
			Vector2 n = q.poll();
			if (cells[(int) n.x][(int) n.y].getState() != Cell.State.DANGEROUS)
				continue;

			Vector2 w = n, e = new Vector2(n.x + 1, n.y);
			while ((w.x > 0) && (cells[(int) w.x][(int) w.y].getState() == Cell.State.DANGEROUS)) {
				cells[(int) w.x][(int) w.y].setState(Cell.State.VULNERABLE);
				cells[(int) w.x][(int) w.y].setMarked(true);
				if ((w.y > 0) && (cells[(int) w.x][(int) (w.y - 1)].getState() == Cell.State.DANGEROUS))
					q.add(new Vector2(w.x, w.y - 1));
				if ((w.y < cells[0].length - 1)
						&& (cells[(int) w.x][(int) (w.y + 1)].getState() == Cell.State.DANGEROUS))
					q.add(new Vector2(w.x, w.y + 1));
				w.x--;
			}
			while ((e.x < cells.length - 1)
					&& (cells[(int) e.x][(int) e.y].getState() == Cell.State.DANGEROUS)) {
				cells[(int) e.x][(int) e.y].setState(Cell.State.VULNERABLE);
				cells[(int) e.x][(int) e.y].setMarked(true);
				if ((e.y > 0) && (cells[(int) e.x][(int) (e.y - 1)].getState() == Cell.State.DANGEROUS))
					q.add(new Vector2(e.x, e.y - 1));
				if ((e.y < cells[0].length - 1)
						&& (cells[(int) e.x][(int) (e.y + 1)].getState() == Cell.State.DANGEROUS))
					q.add(new Vector2(e.x, e.y + 1));
				e.x++;
			}
		}


	}
	
	
	/**
	 * Sets cell states according to the location of the enemies and player.
	 * 
	 * Sets cells that are marked by enemies as dangerous, scores (if necessary sets as SAFE) cells which 
	 * are VULNERABLE or not marked by enemies.
	 * 
	 * @param cells Array of world grid cells
	 */
	private void scoreCells(Cell[][] cells) {
		for (Cell[] cellArray : cells) {
			for(Cell cell : cellArray) {
				if(cell.isMarked()) {
					cell.setState(Cell.State.DANGEROUS);
				}
				if(cell.getState() == Cell.State.VULNERABLE) {
					cell.setState(Cell.State.SAFE);
					if(!cell.isScored()) {
						player.setScore(player.getScore()+1);
						world.setRoundScore(world.getRoundScore()+1);
						cell.setScored(true);
					}

				}

				if(!cell.isMarked()) {
					cell.setState(Cell.State.SAFE);
					if(!cell.isScored()) {
						player.setScore(player.getScore()+1);
						world.setRoundScore(world.getRoundScore()+1);
						cell.setScored(true);
					}
				}
				cell.setMarked(false);
			}
		}
		
		player.setLastSafePosition(null);
		System.out.println("Score: " + player.getScore());
		if(world.getRoundScore() >= 16200) {
			setGameOver(true);
		}
	}

	/**
	 * Handles impact (collision) between player and enemy.
	 * 
	 * Sets player to his last safe position, where he started drawing.
	 * 
	 * @param cells Array of world grid cells
	 */
	private void handleImpact(Cell[][] cells) {
		for (Cell[] cellArray : cells) {
			for(Cell cell : cellArray) {
				if(cell.getState() == Cell.State.VULNERABLE) {
					cell.setState(Cell.State.DANGEROUS);
				}
			}
		}
		
		player.setState(Player.State.IDLE);
		player.setLastSafePosition(null);
		player.setNumOfDeaths(player.getNumOfDeaths()+1);
		keys.get(keys.put(Keys.SHIFT, false));


	}

	/**
	 * Manages enemy movement.
	 * 
	 * Makes enemies bounce if their next movement would be inside a SAFE cell or 
	 * puts the player back into his last safe position, if the next movement would 
	 * be inside a VULNERABLE cell.
	 * 
	 * @param enemies ArrayList of active enemies in the world
	 * @param cells Array of world grid cells
	 */
	private void handleEnemies(ArrayList<Enemy> enemies, Cell[][] cells) {
		int x,y;
		for(Enemy enemy : world.getEnemies()) {
			if(!enemy.isCooldown() && player.getState() == Player.State.DANGER) {
				enemy.shoot(world.getBullets(), player.getPosition());
			}
			
			x = (int) enemy.getPosition().x;
			y = (int) enemy.getPosition().y;
			if(enemy.getDirection() == Enemy.Direction.NE) {
				
				
				if(cells[y][x+Enemy.SIZE/4].getState() == Cell.State.SAFE) {
					enemy.setDirection(Enemy.Direction.NW);
					
				}
				else if(cells[y+Enemy.SIZE/4][x].getState() == Cell.State.SAFE) {
					enemy.setDirection(Enemy.Direction.SE);
				}

				if(cells[y+Enemy.SIZE/4][x+Enemy.SIZE/4].getState() == Cell.State.SAFE &&
						cells[y][x+Enemy.SIZE/4].getState() == Cell.State.SAFE &&
						cells[y+Enemy.SIZE/4][x].getState() == Cell.State.SAFE) {
					enemy.setDirection(Enemy.Direction.SW);
				}


			}
			else if(enemy.getDirection() == Enemy.Direction.SE) {

				if(cells[y][x+Enemy.SIZE/4].getState() == Cell.State.SAFE) {
					enemy.setDirection(Enemy.Direction.SW);
				}
				else if(cells[y-Enemy.SIZE/4][x].getState() == Cell.State.SAFE) {
					enemy.setDirection(Enemy.Direction.NE);
				}
				if(cells[y-Enemy.SIZE/4][x+Enemy.SIZE/4].getState() == Cell.State.SAFE &&
						cells[y][x+Enemy.SIZE/4].getState() == Cell.State.SAFE &&
						cells[y-Enemy.SIZE/4][x].getState() == Cell.State.SAFE) {
					enemy.setDirection(Enemy.Direction.NW);
				}


			}
			else if(enemy.getDirection() == Enemy.Direction.SW) {
				if(cells[y][x-Enemy.SIZE/4].getState() == Cell.State.SAFE) {
					enemy.setDirection(Enemy.Direction.SE);
				}
				else if(cells[y-Enemy.SIZE/4][x].getState() == Cell.State.SAFE) {
					enemy.setDirection(Enemy.Direction.NW);
				}
				if(cells[y-Enemy.SIZE/4][x-Enemy.SIZE/4].getState() == Cell.State.SAFE &&
						cells[y][x-Enemy.SIZE/4].getState() == Cell.State.SAFE &&
						cells[y-Enemy.SIZE/4][x].getState() == Cell.State.SAFE) {
					enemy.setDirection(Enemy.Direction.NE);
				}


			}
			else if(enemy.getDirection() == Enemy.Direction.NW) {
				if(cells[y][x-Enemy.SIZE/4].getState() == Cell.State.SAFE) {
					enemy.setDirection(Enemy.Direction.NE);
				}
				else if(cells[y+Enemy.SIZE/4][x].getState() == Cell.State.SAFE) {
					enemy.setDirection(Enemy.Direction.SW);
				}
				if(cells[y+Enemy.SIZE/4][x-Enemy.SIZE/4].getState() == Cell.State.SAFE &&
						cells[y][x-Enemy.SIZE/4].getState() == Cell.State.SAFE &&
						cells[y+Enemy.SIZE/4][x].getState() == Cell.State.SAFE) {
					enemy.setDirection(Enemy.Direction.SE);
				}

			}
			if(cells[y][x].getState() == Cell.State.VULNERABLE || (enemy.getHitbox().contains(player.getPosition()) && player.getState() == Player.State.DANGER)) {
				player.setPosition(new Vector2(player.getLastSafePosition().x, player.getLastSafePosition().y));
				handleImpact(cells);
			}
			enemy.update();
		}
		for(Bullet bullet : world.getBullets()) { 
			bullet.update();
			if(cells[(int) Math.floor(bullet.getPosition().y)][(int) Math.floor(bullet.getPosition().x)].getState() != Cell.State.DANGEROUS) {
				world.getBullets().remove(bullet);
			}
			if(player.getHitbox().contains(bullet.getPosition()) && player.getState() == Player.State.DANGER) {
				player.setPosition(new Vector2(player.getLastSafePosition().x, player.getLastSafePosition().y));
				handleImpact(cells);
			}
		}
	}

	public boolean isGameOver() {
		return gameOver;
	}

	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}

}
