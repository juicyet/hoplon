package ee.juicy.hoplon.model;

import com.badlogic.gdx.math.Vector2;

public class VerticalLine extends Line{
	
	public VerticalLine(Vector2 start, Vector2 end) {
		super(start, end);
		this.bounds.width = SIZE;
		this.bounds.height = Math.abs(end.y - start.y);
		this.bounds.x = start.x;
		this.bounds.y = start.y;
	}

}
