package ee.juicy.hoplon.model;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Cell extends Rectangle{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public enum State {
		SAFE, DANGEROUS, VULNERABLE, MARKED
	}
	
	public static final int SIZE = 2;
	State		state = State.DANGEROUS;
	Vector2 	position = new Vector2();
	
	boolean isMarked = false;
	boolean isScored = false;
	boolean isDrawn = false;
	
	public boolean isDrawn() {
		return isDrawn;
	}

	public void setDrawn(boolean isDrawn) {
		this.isDrawn = isDrawn;
	}

	public Cell(Vector2 pos) {
		this.x = pos.x;
		this.y = pos.y;
		this.position = pos;
		this.width = SIZE;
		this.height = SIZE;
	}
	
	public boolean isScored() {
		return isScored;
	}

	public void setScored(boolean isScored) {
		this.isScored = isScored;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}
	
	public boolean isMarked() {
		return isMarked;
	}

	public void setMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}
	
	public Vector2 getPosition() {
		return position;
	}

}
