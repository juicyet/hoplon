package ee.juicy.hoplon.model;

import com.badlogic.gdx.math.Vector2;

public class HorizontalLine extends Line{

	public HorizontalLine(Vector2 start, Vector2 end) {
		super(start, end);
		this.bounds.width = Math.abs(end.x - start.x);
		this.bounds.height = SIZE;
		this.bounds.x = start.x;
		this.bounds.y = start.y;
	}

}
