package ee.juicy.hoplon.model;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Player {

	public enum State {
		IDLE, MOVING, DANGER
	}
	
	public enum Direction {
		LEFT, RIGHT, UP, DOWN
	}
	
	public static final float SPEED = 1f;	// unit per second
	public static final float SIZE = 16f;
	
	private Vector2 	position;
	private Vector2		positionOnScreen;
	private Circle		hitbox;
	
	public Vector2 getPositionOnScreen() {
		positionOnScreen.x = World.LEFT_X + position.x * SIZE;
		positionOnScreen.y = World.BOTTOM_Y + position.y * SIZE;
		return positionOnScreen;
	}
	
	private Direction   direction;
	

	private Vector2 	lastSafePosition = null;
	private Vector2 	acceleration = new Vector2();
	private Vector2 	velocity = new Vector2();
	private Rectangle 	bounds = new Rectangle();
	private State		state = State.IDLE;
	private boolean     isSafe = true;
	private int 		score = 0;
	private int			numOfDeaths = 0;
	
	public int getNumOfDeaths() {
		return numOfDeaths;
	}

	public void setNumOfDeaths(int numOfDeaths) {
		this.numOfDeaths = numOfDeaths;
	}

	public Circle getHitbox() {
		hitbox = new Circle(position, SIZE/4);
		return hitbox;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public boolean isSafe() {
		return isSafe;
	}

	public void setSafe(boolean isSafe) {
		this.isSafe = isSafe;
	}

	public Player(Vector2 position) {
		this.position = new Vector2(position.x, position.y);
		//this.lastSafePosition = new Vector2(position.x, position.y);
		this.bounds.height = SIZE;
		this.bounds.width = SIZE;
		this.direction = Direction.UP;
	}	

	public void setLastSafePosition(Vector2 lastSafePosition) {
		this.lastSafePosition = lastSafePosition;
	}

	public Vector2 getPosition() {
		return position;
	}
	
	public Vector2 getLastSafePosition() {
		return lastSafePosition;
	}

	public void setPosition(Vector2 position) {
		this.position = position;
	}

	public Vector2 getAcceleration() {
		return acceleration;
	}

	public Vector2 getVelocity() {
		return velocity;
	}

	public Rectangle getBounds() {
		bounds.x = position.x;
		bounds.y = position.y;
		return bounds;
	}

	public State getState() {
		return state;
	}
	
	public void setState(State newState) {
		this.state = newState;
	}
}
