package ee.juicy.hoplon.model;

import java.util.Random;

import com.badlogic.gdx.math.Vector2;

public class Bullet {

	Vector2 	position;
	Random 		rnd = new Random();
	Line		trajectory;

	public static final int SPEED = 1;

	double verticalSpeed;
	double horizontalSpeed;

	public Bullet(Vector2 position, Vector2 playerPos) {
		super();
		this.position = position;
		verticalSpeed = Math.cos(Math.atan2(position.x-playerPos.x, position.y-playerPos.y))*SPEED;
		horizontalSpeed = Math.sin(Math.atan2(position.x-playerPos.x, position.y-playerPos.y))*SPEED;
	}

	public void update() {
		position.x -= horizontalSpeed;
		position.y -= verticalSpeed;
	}

	public Vector2 getPosition() {
		return position;
	}




}
