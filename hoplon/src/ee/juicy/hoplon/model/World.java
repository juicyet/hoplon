package ee.juicy.hoplon.model;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

import com.badlogic.gdx.math.Vector2;

import ee.juicy.hoplon.Hoplon;

public class World {

	Cell[][] cells = new Cell[Hoplon.ARENA_HEIGHT/2][Hoplon.ARENA_WIDTH/2];
	CopyOnWriteArrayList<Bullet> bullets = new CopyOnWriteArrayList<Bullet>();
	
	public CopyOnWriteArrayList<Bullet> getBullets() {
		return bullets;
	}
	public Cell[][] getCells() {
		return cells;
	}
	/** Our player controlled hero **/
	Player player;
	ArrayList<Enemy> enemies;
	
	private int width, height;
	private int difficultyLevel;
	private int cellCount = 0;
	private int roundScore = 0;
	private int numOfDeaths = 0;
	
	
	public static final float BOTTOM_Y = (Hoplon.SCREEN_HEIGHT - Hoplon.ARENA_HEIGHT)/2, 
			TOP_Y = (Hoplon.SCREEN_HEIGHT - Hoplon.ARENA_HEIGHT)/2 + Hoplon.ARENA_HEIGHT, 
			LEFT_X = (Hoplon.SCREEN_WIDTH - Hoplon.ARENA_WIDTH)/2, 
			RIGHT_X = (Hoplon.SCREEN_WIDTH - Hoplon.ARENA_WIDTH)/2 + Hoplon.ARENA_WIDTH;
	

	public Player getPlayer() {
		return player;
	}
	public World(int difficultyLevel) {
		this.difficultyLevel = difficultyLevel;
		createDemoWorld();
		
	}

	private void createDemoWorld() {
		
		enemies = new ArrayList<Enemy>();
		if(difficultyLevel == 1) {
			enemies.add(new Enemy(new Vector2(20,20)));
		}		
		else if(difficultyLevel == 2) {
			enemies.add(new Enemy(new Vector2(20,20)));
			enemies.add(new Enemy(new Vector2(100,100)));
		}		
		else if(difficultyLevel == 3) {
			enemies.add(new Enemy(new Vector2(20,20)));
			enemies.add(new Enemy(new Vector2(100,100)));
			enemies.add(new Enemy(new Vector2(60,40)));
		}
		else if(difficultyLevel == 4) {
			enemies.add(new Enemy(new Vector2(20,20)));
			enemies.add(new Enemy(new Vector2(100,100)));
			enemies.add(new Enemy(new Vector2(60,40)));
			enemies.add(new Enemy(new Vector2(50,50)));
		}
		else if(difficultyLevel == 5){
			enemies.add(new Enemy(new Vector2(20,20)));
			enemies.add(new Enemy(new Vector2(100,100)));
			enemies.add(new Enemy(new Vector2(60,40)));
			enemies.add(new Enemy(new Vector2(50,50)));
			enemies.add(new Enemy(new Vector2(80,20)));
		}
		else if(difficultyLevel == 6){
			enemies.add(new Enemy(new Vector2(20,20)));
			enemies.add(new Enemy(new Vector2(100,100)));
			enemies.add(new Enemy(new Vector2(60,40)));
			enemies.add(new Enemy(new Vector2(50,50)));
			enemies.add(new Enemy(new Vector2(80,20)));
			enemies.add(new Enemy(new Vector2(40,35)));
		}
		else {
			enemies.add(new Enemy(new Vector2(20,20)));
			enemies.add(new Enemy(new Vector2(100,100)));
			enemies.add(new Enemy(new Vector2(60,40)));
			enemies.add(new Enemy(new Vector2(50,50)));
			enemies.add(new Enemy(new Vector2(80,20)));
			enemies.add(new Enemy(new Vector2(40,35)));
			enemies.add(new Enemy(new Vector2(75,90)));
		}
		
		player = new Player(new Vector2(0, 0));
		
		//add initial bounding lines
		for(int i = 0; i < Hoplon.ARENA_HEIGHT/2; i++) {
			for(int j = 0; j < Hoplon.ARENA_WIDTH/2; j++) {
				cells[i][j] = new Cell(new Vector2(j,i));
				if(i==0 || j == 0 || i == Hoplon.ARENA_HEIGHT/2-1 || j == Hoplon.ARENA_WIDTH/2-1) {
					cells[i][j].setState(Cell.State.SAFE);
				}
			}
		}
		
		
		for (Cell[] cellArray : cells) {
			for(Cell cell : cellArray) {
				if(cell != null) {
					cellCount++;
				}
			}
		}
		System.out.println("No. of cells in grid: " + cellCount);
		
		
		
	}
	public int getCellCount() {
		return cellCount;
	}
	public ArrayList<Enemy> getEnemies() {
		return enemies;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getDifficultyLevel() {
		return difficultyLevel;
	}
	public int getRoundScore() {
		return roundScore;
	}
	public void setRoundScore(int roundScore) {
		this.roundScore = roundScore;
	}
	public int getNumOfDeaths() {
		return numOfDeaths;
	}
	public void setNumOfDeaths(int numOfDeaths) {
		this.numOfDeaths = numOfDeaths;
	}
}
