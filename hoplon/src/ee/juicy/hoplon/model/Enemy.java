package ee.juicy.hoplon.model;

import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Enemy{
	
	public enum Direction {
		NE, SE, SW, NW
	}
	
	public static final float SPEED = 1f;	// unit per second
	public static final int SIZE = 16;
	
	Vector2 	position;
	Rectangle 	bounds = new Rectangle();
	Direction   direction;
	Random 		rnd = new Random();
	Circle		hitbox;
	boolean		isCooldown;
	long		coolTime;

	public Circle getHitbox() {
		hitbox = new Circle(position, SIZE/2);
		return hitbox;
	}

	public Enemy(Vector2 position) {
		this.position = new Vector2(position.x, position.y);
		this.bounds.height = SIZE;
		this.bounds.width = SIZE;
		this.direction = Direction.values()[rnd.nextInt(Direction.values().length)];
		this.isCooldown = false;
		this.coolTime = System.currentTimeMillis();
		
	}
	
	public Direction getDirection() {
		return direction;
	}


	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public Vector2 getPosition() {
		return position;
	}

	public Rectangle getBounds() {
		bounds.x = position.x;
		bounds.y = position.y;
		return bounds;
	}

	/**
	 * Moves the enemy according to current direction.
	 */
	public void update() {
		if(System.currentTimeMillis() - coolTime > 2000 && isCooldown == true) {
			isCooldown = false;
			coolTime = System.currentTimeMillis();
		}
			
			if(direction == Direction.NE) {
			position.x += SPEED;
			position.y += SPEED;
		}
		else if(direction == Direction.SE) {
			position.x += SPEED;
			position.y -= SPEED;
		
		}
		else if(direction == Direction.SW) {
			position.x -= SPEED;
			position.y -= SPEED;
		
		}
		else if(direction == Direction.NW) {
			position.x -= SPEED;
			position.y += SPEED;
		
		}
		
	}
	
	public boolean isCooldown() {
		return isCooldown;
	}

	public void shoot(CopyOnWriteArrayList<Bullet> copyOnWriteArrayList, Vector2 playerPos) {
		if(isCooldown == false) {
			copyOnWriteArrayList.add(new Bullet(new Vector2(position.x, position.y), playerPos));
			isCooldown = true;
		}
	}

}
