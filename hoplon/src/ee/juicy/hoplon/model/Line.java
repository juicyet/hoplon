package ee.juicy.hoplon.model;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Line {
	
public static final float SIZE = 0;
	
	Vector2 	start = new Vector2();
	Vector2 	end = new Vector2();
	Rectangle 	bounds = new Rectangle();
	
	public Line(Vector2 start, Vector2 end) {
		this.start = start;
		this.end = end;
		this.bounds.x = start.x;
		this.bounds.y = start.y;
	}

	public Vector2 getStart() {
		return start;
	}

	public Vector2 getEnd() {
		return end;
	}

	public Rectangle getBounds() {
		return bounds;
	}

}
